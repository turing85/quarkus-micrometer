# quarkus-micrometer Project
This project shows:
- how to set up micrometer in quarkus with prometheus exporter
- how to configure prometheus and grafana with a dashboard


## Build quarkus application
```
./mvnw package
```

## Deploy application, Prometheus and Grafana
```
cd localdeployment
docker compose up --build -d
```

## Open Grafana and access dashboard
The web-ui is available at [`localhost:3000`](http://localhost:3000). Credentials are `grafana/grafana`. After login, we can access the dashboard by clicking on "CPU and Memory".

## Generate some load
We can generate some load through, e.g. `curl`:
```
for i in {1..10}; do \
  for i in {1..1000}; do { 
    curl -s 'localhost:8080/hello' > /dev/null
  }; done \
& done 
```

## Review metrics in Grafana
We should see the generated load in Grafana :)